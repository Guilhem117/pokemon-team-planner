# README #

### How to launch? ###
To launch the project, with a command line do (in the repository) :
    initMongoDB.bat
	node server.js

### Metadata ###

* Pokemon Team Planner
* Author : SERENE Guilhem
* License : Copyleft (Free rights)
* Version 0.7.1
* Demo of 25/11 - 14h15

### Configuration required ###

* Summary of set up
	A recent browser 
	Internet required to make some calls to pokeapi
* Dependencies
	All dependencies are in the folder, no external dependancies
	For libraries see package.json

### Tools/Languages ###
NodeJS + Express
MongoDB + JSON
HTML CSS(Bootstrap) JS AJAX 
BitBucket

	
### Contributions ###
Sprites (Pokemon & Types) : play.pokemonshowdown.com/
Custom CSS for Bootstrap : /work.smarchal.com/twbscolor/